package io.gitlab.aacl.microservices.api;

import lombok.Data;
import org.springframework.data.annotation.Id;

@Data
public class Review {
    @Id
    private Integer id;
    private String review;

    private String email;

    private String name;

    public Review() {}

    public Review(RequestDto dto){
        review = dto.getMessage();
        email = dto.getEmail();
        name = dto.getName();
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
