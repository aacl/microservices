package io.gitlab.aacl.microservices.api;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface ReviewRepository extends ReactiveCrudRepository<Review, Integer> {
}
