package io.gitlab.aacl.microservices.api;

import java.util.ArrayList;
import java.util.List;

public class GenericResponse<T> {
    private T result;
    private List<Object> errors;

    private GenericResponse(T rsp) {
        result = rsp;
        errors = new ArrayList<>();
    }

    private GenericResponse(List<Object> errors) {
        this.errors = errors;
    }

    private GenericResponse<T> addError(Object error) {
        errors.add(error);
        return this;
    }

    public T getResult() {
        return result;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public static <T> GenericResponse<T> ofResult(T result) {
        return new GenericResponse<T>(result);
    }

    public static <T> GenericResponse<T> ofError(T result) {
        return new GenericResponse<T>(result);
    }
}
