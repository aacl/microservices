package io.gitlab.aacl.microservices.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
public class Controller {

    private ReviewRepository repo;

    @Autowired
    public void setReviewRepository(ReviewRepository repo) {
        this.repo = repo;
    }


    @PostMapping("/review")
    public Mono<GenericResponse<String>> handle(@RequestBody RequestDto dto) {

        return repo.save(new Review(dto))
                   .map(x -> GenericResponse.ofResult("Thank you for your response " + x.getName()));
    }

    @GetMapping("actuator/health")
    public ResponseEntity healthCheck() {
        return new ResponseEntity(HttpStatus.OK);
    }
}
